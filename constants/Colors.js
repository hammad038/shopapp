export default {
  brown: '#5D4A43',
  cream: '#C7AA9B',
  black: '#000000',
  white: '#ffffff',
  red: '#e73353',
};
