import React, {useState} from 'react';
import {View, Text, StyleSheet, ScrollView, Image, Button} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import Colors from '../../constants/Colors';
import * as cartActions from '../../store/actions/cart';

const ProductDetailScreen = (props) => {
  // get the product id, which was selected on ProductsOverviewScreen
  const productId = props.navigation.getParam('productId');
  // find the product with id from the redux store
  const selectedProduct = useSelector((state) =>
    state
      .products
      .availableProducts
      .find(prod => prod.id === productId));

  const dispatch = useDispatch();

  return (
    <ScrollView style={styles.screen}>
      <Image source={{uri: selectedProduct.imageUrl}} style={styles.image}/>

      <View style={styles.actions}>
        <Button
          title='Add to Cart'
          color={Colors.cream}
          onPress={() => {
            dispatch(cartActions.addToCart(selectedProduct))
          }}
        />
      </View>

      <Text style={styles.price}>${selectedProduct.price}</Text>

      <Text style={styles.description}>{selectedProduct.description}</Text>
    </ScrollView>
  );
};

ProductDetailScreen.navigationOptions = (navData) => {
  return ({
    headerTitle: navData.navigation.getParam('productTitle'),
  });
}

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 300,
  },
  actions: {
    marginVertical: 10,
    alignItems: 'center',
  },
  price: {
    fontSize: 20,
    color: Colors.red,
    textAlign: 'center',
    marginVertical: 20,
    fontFamily: 'open-sans-bold',
  },
  description: {
    fontFamily: 'open-sans',
    fontSize: 14,
    textAlign: 'center',
    marginHorizontal: 20,
  },
});

export default ProductDetailScreen;
