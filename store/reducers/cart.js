import {ADD_TO_CART, REMOVE_FROM_CART} from '../actions/cart';
import {ADD_ORDER} from '../actions/order';
import CartItem from '../../models/cart-item';
import {DELETE_PRODUCT} from '../actions/products';

const initialState = {
  items: {},
  totalAmount: 0,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      const addedProduct = action.product;
      const productPrice = addedProduct.price;
      const productTitle = addedProduct.title;
      let updateOrNewCartItem;

      if(state.items[addedProduct.id]) {
        // added product is already in the cart
        updateOrNewCartItem = new CartItem(
          state.items[addedProduct.id].quantity + 1,
          productPrice,
          productTitle,
          state.items[addedProduct.id].sum + productPrice
        );
      } else {
        // adding a new item in cart
        updateOrNewCartItem = new CartItem(1, productPrice, productTitle, productPrice);
      }

      return {
        ...state,
        items: {
          ...state.items,
          [addedProduct.id]: updateOrNewCartItem,
        },
        totalAmount: state.totalAmount + productPrice,
      };

    case REMOVE_FROM_CART:
      const productId = action.pid;
      const selectedCartItem = state.items[productId];
      const currentQuantity = selectedCartItem.quantity;
      let updatedCartItems;

      if(currentQuantity > 1) {
        // reduce item quantity by 1 if there are many items in cart of same type
        const updatedCartItem = new CartItem(
          currentQuantity - 1,
          selectedCartItem.price,
          selectedCartItem.title,
          selectedCartItem.sum - selectedCartItem.price
          );

        updatedCartItems = {...state.items, [productId]: updatedCartItem}

      } else {
        // remove item from cart
        updatedCartItems = {...state.items};
        delete updatedCartItems[productId];
      }

      return {
        ...state,
        items: updatedCartItems,
        totalAmount: state.totalAmount - selectedCartItem.price
      }

    case ADD_ORDER:
      return initialState;

    case DELETE_PRODUCT:
      // if deleted product is not in the cart
      if(!state.items[action.pid]) {
        return state;
      }

      // if deleted product also exist in the cart
      const updatedItems = {...state.items};
      const itemTotal = state.items[action.pid].sum;
      delete updatedItems[action.pid];

      return {
        ...state,
        items: updatedItems,
        totalAmount: state.totalAmount - itemTotal,
      }
  }

  return state;
}
