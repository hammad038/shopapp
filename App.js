import React, {useState} from 'react';
import {Text, View} from 'react-native';
import {AppLoading} from 'expo';
import {enableScreens} from 'react-native-screens';
import ReduxThunk from 'redux-thunk';

// react-redux imports
import {createStore, combineReducers, applyMiddleware} from 'redux';
import productsReducer from './store/reducers/products';
import cartReducer from './store/reducers/cart';
import ordersReducer from './store/reducers/order';
import authReducer from './store/reducers/auth';
import {Provider} from 'react-redux';

// loading fonts
import * as Font from 'expo-font';
const fetchFonts = () => {
  return Font.loadAsync({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf'),
  });
}

// make app performance efficient
enableScreens();

// redux setup for creating store
const rootReducer = combineReducers({
  products: productsReducer,
  cart: cartReducer,
  orders: ordersReducer,
  auth: authReducer,
});
const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

// import navigation
import NavigationContainer from './navigation/NavigationContainer';

// app component
export default function App() {
  const [fontLoaded, setFontLoaded] = useState(false);

  // check if fonts loaded successfully before app starts
  if (!fontLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setFontLoaded(true)}
      />);
  }

  return (
    <Provider store={store}>
      <NavigationContainer />
    </Provider>
  );
}
