import React, {useState} from 'react';
import {Platform} from 'react-native';
import {HeaderButton} from 'react-navigation-header-buttons';
import {Ionicons} from '@expo/vector-icons';
import Colors from '../../constants/Colors';

const CustomHeaderButton = (props) => {
  return (
    <HeaderButton
      {...props}
      IconComponent={Ionicons}
      IconSize={25}
      color={Platform.OS === 'android' ? Colors.white : Colors.brown}
    />
  );
};

export default CustomHeaderButton;
