import React from 'react';
import {Platform, SafeAreaView, Button, View} from 'react-native';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createDrawerNavigator, DrawerItems} from 'react-navigation-drawer';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {Ionicons} from '@expo/vector-icons';
import Colors from '../constants/Colors';
import {useDispatch} from 'react-redux';
import * as authAction from '../store/actions/auth';

// screens imports
import ProductsOverviewScreen from '../screens/shop/ProductsOverviewScreen';
import ProductDetailScreen from '../screens/shop/ProductDetailScreen';
import OrdersScreen from '../screens/shop/OrdersScreen';
import CartScreen from '../screens/shop/CartScreen';
import UserProductsScreen from '../screens/user/UserProductsScreen';
import EditProductScreen from '../screens/user/EditProductScreen';
import AuthScreen from '../screens/user/AuthScreen';
import StartupScreen from '../screens/StartupScreen';

// default navigation options
const defaultNavOptions = {
  headerStyle: {
    backgroundColor: Platform.OS === 'android' ? Colors.brown : '',
  },
  headerTitleStyle: {
    fontFamily: 'open-sans-bold'
  },
  headerBackTitleStyle: {
    fontFamily: 'open-sans'
  },
  headerTintColor: Platform.OS === 'android' ? Colors.white : Colors.brown,
};

// products navigator
const ProductsNavigator = createStackNavigator({
  ProductsOverview: ProductsOverviewScreen,
  ProductDetail: ProductDetailScreen,
  Cart: CartScreen,
}, {
  defaultNavigationOptions: defaultNavOptions,
  navigationOptions: {
    drawerIcon: drawerConfig => (
      <Ionicons
        name={Platform.OS === 'android' ? 'md-cart' : 'ios-cart'}
        size={25}
        color={drawerConfig.tintColor}
      />)
  },
});

// orders navigator
const OrdersNavigator = createStackNavigator({
  Orders: OrdersScreen,
}, {
  defaultNavigationOptions: defaultNavOptions,
  navigationOptions: {
    drawerIcon: drawerConfig => (
      <Ionicons
        name={Platform.OS === 'android' ? 'md-list' : 'ios-list'}
        size={25}
        color={drawerConfig.tintColor}
      />)
  },
});

// users navigator - drawer
const AdminNavigator = createStackNavigator({
  UserProducts: UserProductsScreen,
  EditProduct: EditProductScreen,
}, {
  defaultNavigationOptions: defaultNavOptions,
  navigationOptions: {
    drawerIcon: drawerConfig => (
      <Ionicons
        name={Platform.OS === 'android' ? 'md-create' : 'ios-create'}
        size={25}
        color={drawerConfig.tintColor}
      />)
  },
});

const ShopNavigator = createDrawerNavigator({
  Products: ProductsNavigator,
  Order: OrdersNavigator,
  Admin: AdminNavigator,
}, {
  contentOptions: {
    activeTintColor: Colors.brown,
  },
  contentComponent: props => {
    const dispatch = useDispatch();
    return (
      <View style={{flex: 1, paddingTop: 20}}>
        <SafeAreaView forceInset={{top: 'always', horizontal: 'never'}}>
          <DrawerItems {...props} />
          <Button
            title='Logout'
            color={Colors.brown}
            onPress={() => {
              dispatch(authAction.logout());
            }}
          />
        </SafeAreaView>
      </View>
    );
  }
});

const AuthNavigator = createStackNavigator({
  Auth: AuthScreen,
}, {
  defaultNavigationOptions: defaultNavOptions
});

const MainNavigator = createSwitchNavigator({
  Startup: StartupScreen,
  Auth: AuthNavigator,
  Shop: ShopNavigator,
});

export default createAppContainer(MainNavigator);
